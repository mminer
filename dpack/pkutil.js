function ArrCompare (a0, a1) {
  if (a0.length != a1.length) {
    print("length differs!");
    print("l0: "+a0.length);
    print("l1: "+a1.length);
    return;
  }
  for (var f = 0; f < a1.length; f++) {
    var c0 = a0[f], c1 = a1[f];
    if (c0 == c1) continue;
    print(f+": 0x"+c0.toString(16)+" 0x"+c1.toString(16)+"  "+c0+" "+c1);
  }
}


function ArrLength (arr) {
  var res = 0;
  for (var f = 0; f < arr.length; f++) {
    var t = ""+arr[f];
    res += t.length+1;
  }
  return res;
}
