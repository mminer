/*
 * simple RLE packer
 * 1st char: code of rep prefix, then:
 * char or
 * repprefix,count,code
 */
function RLEPack (s) {
  var f, res = "", used = [], ch;
  /* find least used char */
  for (f = 0; f <= 255; f++) used[f] = 0;
  for (f = 0; f < s.length; f++) {
    ch = s[f];
    /*if (!used[ch]) {
      if (ch >= 32 && ch <= 126) print('"'+String.fromCharCode(ch)+'"');
      else print("0x"+ch.toString(16).toUpperCase());
    }*/
    used[ch]++;
  }

  var repChar = 35, repCnt = used[repChar], repASCII = true;
  for (f = 0; f <= 255; f++) {
    var uc = used[f];
    if (uc < repCnt || (!repASCII && uc <= repCnt)) {
      repChar = f; repCnt = uc;
      repASCII = (f >= 32 && f <= 126 && f != 34 && f != 92);
    }
  }
  /*
  print("----------");
  if (repASCII) print('"'+String.fromCharCode(repChar)+'"');
  else print("0x"+repChar.charCodeAt(0).toString(16).toUpperCase());
  print("cnt: "+repCnt);
  print("used: "+used[repChar]);
  */
  function AddCode (c, isRep) {
    if (!isRep && c == repChar) {
      /* 'repeat' once */
      /*print("fuck!");*/
      AddCode(repChar, true);
      AddCode(0, true);
    }
    if (c >= 32 && c <= 126) {
      if (c == 34 || c == 92) res += '\\';
      res += String.fromCharCode(c);
    } else {
      var h = c.toString(16).toLowerCase();
      while (h.length < 2) h = "0"+h;
      res += "\\x"+h;
    }
  }

  AddCode(repChar, true); /* repchar */
  for (f = 0; f < s.length; ) {
    var c = s[f];
    if (f >= s.length-2) { AddCode(c); f++; continue; }
    if (s[f+1] == c && (c == repChar || s[f+2] == c)) {
      /* repeats */
      AddCode(repChar, true);
      var cnt = -1; /* 0 means at least one repeat */
      while (cnt < 255 && f < s.length && s[f] == c) { cnt++; f++; }
      AddCode(cnt, true); /* repcount */
      AddCode(c, true); /* char */
      /*print("rep "+(cnt+1)+"; char: "+c);*/
    } else { AddCode(c); f++; }
  }
  return res;
}


/*
 * simple RLE unpacker
 * 1st char: code of rep prefix, then:
 * char or
 * repprefix,count,code
 */
function RLEUnpack (s) {
  var res = [], rc = s.charCodeAt(0);
  /*print(rc);*/
  for (var f = 1; f < s.length; ) {
    var cc = s.charCodeAt(f++);
    if (cc == rc) {
      var cnt = s.charCodeAt(f++);
      cc = s.charCodeAt(f++);
      /*print("pos: "+(f-3)+"; rep "+(cnt+1)+"; char: "+cc+"; npos: "+f+"; nchar: "+s.charCodeAt(f));*/
      while (cnt-- > 0) res.push(cc);
    }
    res.push(cc);
  }
  return res;
}


function PackArray (arr) {
  return RLEPack(arr);
}


function DUnpackStr (s) {
  return RLEUnpack(s);
}
