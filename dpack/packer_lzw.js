/*
 * packer/unpacker code derived from JavaScript O Lait library (jsolait)
 * JavaScript O Lait library(jsolait): Copyright (c) 2003-2006 Jan-Klaas Kollhof, GNU LGPL v2.1+
 *
 * modified by Ketmar // Avalon Group
*/
function LZWPack (str) {
  var dict = {};
  var data = (str+"").split("");
  var out = [];
  var currChar;
  var phrase = data[0];
  var code = 256;

  for (var i = 1; i < data.length; i++) {
    currChar = data[i];
    if (dict[phrase+currChar] != null) phrase += currChar;
    else{
      out.push(phrase.length>1?dict[phrase]:phrase.charCodeAt(0));
      dict[phrase+currChar] = code;
      code++;
      phrase = currChar;
    }
  }
  out.push(phrase.length>1?dict[phrase]:phrase.charCodeAt(0));

  var res = "";

  function AddCode (c) {
    if (c >= 32 && c <= 126) {
      if (c == 34 || c == 92) res += '\\';
      res += String.fromCharCode(c);
    } else {
      var h = c.toString(16).toLowerCase();
      while (h.length < 2) h = "0"+h;
      if (h.length == 2) res += "\\x"+h;
      else {
        while (h.length < 4) h = "0"+h;
        res += "\\u"+h;
      }
    }
  }

  for (var i = 0; i < out.length; i++) AddCode(out[i]);
  return res;
}


function LZWUnpack (str) {
  var dict = {};
  var data = (str+"").split("");
  var currChar = data[0];
  var oldPhrase = currChar;
  var out = [currChar];
  var code = 256;
  var phrase;
  for (var i = 1; i < data.length; i++) {
    var currCode = data[i].charCodeAt(0);
    if (currCode < 256) phrase = data[i];
    else phrase = dict[currCode]?dict[currCode]:(oldPhrase+currChar);
    out.push(phrase);
    currChar = phrase.charAt(0);
    dict[code] = oldPhrase+currChar;
    code++;
    oldPhrase = phrase;
  }
  var s = out.join("");
  var res = [];
  for (var f = 0; f < s.length; f++) res.push(s.charCodeAt(f));
  return res;
}


function PackArray (arr) {
  var s = "";
  for (var f = 0; f < arr.length; f++) s += String.fromCharCode(arr[f]);
  return LZWPack(s);
}


function DUnpackStr (s) {
  return LZWUnpack(s);
}
