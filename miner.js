window.levelsets = [];


if (typeof(opera) == "undefined") {
  opera = { postError: function () { return; } };
}



function ManicMiner () {
  var DEBUG_COLDET = false, dbgColDetected = false;

  var willyJ = [0, -4, -4, -3, -3, -2, -2, -1, -1, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4];
  var skylabCoords = [
    [{x: 8,y:0}, {x: 72,y:0}, {x:136,y:0}, {x:200,y:0}],
    [{x:40,y:0}, {x:104,y:0}, {x:168,y:0}, {x:232,y:0}],
    [{x:24,y:0}, {x: 88,y:0}, {x:152,y:0}, {x:216,y:0}]
  ];
  var scrollText = ".  .  .  .  .  .  .  .  .  .  . "+
    "MANIC MINER . . "+
    "\u00a9 BUG-BYTE Ltd. 1983 . . "+
    "By Matthew Smith . . . "+
    "P = Pause . . . "+
    "Guide Miner Willy through 20 lethal caverns "+
    ".  .  .  .  .  .  .  .";


  function unpackRoom (roomS) {
    var room = {}, pos = 0;

    function getNum () {
      var n = roomS.charCodeAt(pos++)-48, len;
      var len = n>>4; n = n&0x0f;
      while (len--) n = n*10+roomS.charCodeAt(pos++)-48;
      return n;
    }

    function getIP (arr) {
      arr.ink = getNum();
      arr.paper = getNum();
    }

    function getGIP (arr) {
      arr.gfx = getNum();
      getIP(arr);
    }

    // map
    room.map = [];
    for (var f = 0; f < 512; ) {
      var bt = roomS.charCodeAt(pos++)-48;
      var cnt = roomS.charCodeAt(pos++)-31;
      while (cnt--) { room.map.push(bt); f++; }
    }
    // params and Willy start
    room.air = getNum();
    room.willy = { x:getNum(), y:getNum(), sd:getNum() };
    room.exit = { gfx:getNum(), x:getNum(), y:getNum() };
    room.border = getNum();
    getIP(room);
    // platforms
    var cnt = getNum();
    room.platforms = [];
    while (cnt--) {
      var t = {}; getGIP(t);
      room.platforms.push(t);
    }
    room.wall = {}; getGIP(room.wall);
    room.crumb = {}; getGIP(room.crumb);
    // deadlies
    var cnt = getNum();
    room.deadlies = [];
    while (cnt--) {
      var t = {}; getGIP(t);
      room.deadlies.push(t);
    }
    // conveyor
    room.conveyor = { x:getNum(), y:getNum(), d:getNum(), l:getNum() };
    getGIP(room.conveyor);
    // keys
    cnt = getNum();
    //LogPrint("keys: "+cnt);
    room.keys = { gfx: getNum(), info:[] };
    for (var f = 0; f < 5; f++) {
      var t = {};
      if (f < cnt) t = { x:getNum(), y:getNum(), s: getNum() };
      else t = { x:-1, y:-1, s:0 };
      room.keys.info.push(t);
    }
    // switches
    cnt = getNum();
    room.switches = [];
    for (var f = 0; f < 2; f++) {
      var sw;
      if (f < cnt) sw = { x: getNum(), y:getNum(), s:getNum() };
      else sw = { x: 0, y: 0, s: 0 };
      room.switches.push(sw);
    }
    // enemies
    cnt = getNum();
    room.enemies = [];
    for (var f = 0; f < 8; f++) {
      var e;
      if (f < cnt) {
        e = {};
        e.vert = roomS.charCodeAt(pos++)=="V";
        getGIP(e);
        e.x = getNum();
        e.y = getNum();
        e.min = getNum();
        e.max = getNum();
        e.d = getNum();
        e.s = getNum();
        e.flip = getNum();
        e.anim = getNum();
      } else {
        e = { vert: true, gfx: 0, ink: 1, paper: 0, x: -1, y: -1, min: 2, max: 81, d: 0, s: 111, flip: 0, anim: 0 };
      }
      room.enemies.push(e);
    }
    // title
    room.title = roomS.substr(pos);

    return room;
  }

  /*
   * unpacker code derived from JavaScript O Lait library (jsolait)
   * JavaScript O Lait library(jsolait): Copyright (c) 2003-2006 Jan-Klaas Kollhof, GNU LGPL v2.1+
   *
   * modified by Ketmar // Avalon Group
  */
  function lzwUnpack (str) {
    var dict = {};
    var data = (str+"").split("");
    var currChar = data[0];
    var oldPhrase = currChar;
    var out = [currChar];
    var code = 256;
    var phrase;
    for (var i = 1; i < data.length; i++) {
      var currCode = data[i].charCodeAt(0);
      if (currCode < 256) phrase = data[i];
      else phrase = dict[currCode]?dict[currCode]:(oldPhrase+currChar);
      out.push(phrase);
      currChar = phrase.charAt(0);
      dict[code] = oldPhrase+currChar;
      code++;
      oldPhrase = phrase;
    }
    var s = out.join("");
    var res = [];
    for (var f = 0; f < s.length; f++) res.push(s.charCodeAt(f));
    return res;
  }

  var me = this, canvas, ctx, mainX, mainY, mainPal;
  this.lsets = [];
  this.setData = function (data) {
    this.data = data;
    var k;
    for (k in data) {
      var v = data[k];
      if (typeof(v) == "string") data[k] = lzwUnpack(v);
    }
  };
  this.addLevelSet = function (lset) {
    if (lset.packed) {
      delete lset.packed;
      var r = lset.rooms;
      lset.rooms = [];
      for (var f = 0; f < r.length; f++) lset.rooms.push(unpackRoom(r[f]));
    }
    this.lsets.push(lset);
  };

  var gameTID;

  var kbdActions; // will be inited later
  var kLeft, kRight, kJump, kUp, kDown;

  var kong, eugene, skylab, switches, spg;
  var finalSpr, sunSpr, kongSpr, eugeneSpr, skylabSpr, switchesSpr;
  var holeLen, holeY;

  var willySpr, willyX, willyY, willyDir, willyJump, willyJumpDir;
  var willyLastMoveDir, willyFall, willyDead, willyConv, willyStall;
  var brickCache, convCache, monsterCache, monsterOfsCache, keyCache, exitCache;
  var curLSet = 0, curRoomNo = 0, curRoom, curMap, curKeys, keysLeft, curMonsters;
  var conv;
  var frameNo;
  var score;

  var gameRunning;

  var dbgNF, dbgSingleStep;


function removeMessage (force) {
  var msg = document.getElementById("k8PageMesssage");
  if (!msg) return;
  if (msg.k8ClickToRemove && !force) return;
  msg.style.display = "none";
  window.removeEventListener("resize", msg.k8FNRS, false);
  setTimeout(function () {
    //opera.postError("msg: "+msg);
    if (!msg || !msg.parentNode) return;
    msg.parentNode.removeChild(msg);
  }, 1);
}


function message (msgs, clickToRemove) {
  while (!msgs) msgs = rndMessages[Math.ceil(Math.random()*rndMessages.length)];
  var msg = document.getElementById("k8PageMesssage");
  if (!msg) {
    var db = document.body;
    var fc = db.firstChild;
    var msg = document.createElement("k8_msg_div");
    msg.id = "k8PageMesssage";
    msg.setAttribute("style",
      "z-index:7779;;border:1px solid #fff;padding:3px;background:#222;"+
      "color:#f"+(clickToRemove?"00":"ff")+";display:block;"+
      "position:fixed;left:-1000px;top:-1000px;"+
      "font-size:28px;font-family:Verdana Tahoma;font-weight:normal;font-style:normal;");
    var img = msg.appendChild(document.createElement("img"));
    img.width = 27; img.height = 27;
    img.setAttribute("style", "margin-left:4px;margin-top:5px;");
    img.src = "data:image/gif;base64,"+ //27x27
      "R0lGODlhGwAbAKEBAAAAAP///////////yH/C05FVFNDQVBFMi4wAwEAAAAh/iNvcHBvc2l0"+
      "ZXMgYnkgS2V0bWFyIC8vIEF2YWxvbiBHcm91cAAh+QQJCgACACwAAAAAGwAbAAACbZSPoRvo"+
      "fxSYFCgIZd22YRNw4uRl41km55oK4cqqMBrNsPfOTG65dreraG67BWUo3DCAOSQJaXxqJDyL"+
      "0hjsWFnFJbUq7Hp9Tqk4hANez9Qa+9xzv7tx+RxrurcxrzfpA9JX9ASYANZRmFEEWAAAIfkE"+
      "CQoAAwAsAAAAABsAGwAAAm6cj6Eb6H8UmBQoCGXdtmETcOLkZeNZJme3VOkQokxLquOMW9HK"+
      "sL0Xu2l8kkZQWMSFhkhli9l0dlYU5/PIs05FWOsSS2J6dWDLRgukhpU6EBVti87aO7ML/vh6"+
      "n5/YvvYB45fTEZhQdmGYhxNYAAAh+QQJCgADACwAAAAAGwAbAAACcJyPoRvofxSYFCgIZZ36"+
      "YhNsHBNaX1mRpPmglKqykQjD8uCOUh02oGjRBG2+nK7HMxmPvEUQyGw+ocMmkkqrTbEp4hLr"+
      "5QJjje9rsyqChdXejJpj3Mxjp+8tlt8TeftpXbKXYeQU9OHA9iJ4GKFyWAAAIfkECQoAAwAs"+
      "AAAAABsAGwAAAm6cj6Eb6H8UmBQoCKVl+mITTNwofuE2ks1zoilnOe1LxxFFv/Yw5/rak/gW"+
      "sRYKNwxpRMSk0FirqIwuYoXZpFav0iz3C/aCudQmb9yFKkFo6VV4a3/XcXl3x7a/M3qcqb9i"+
      "ovUW+AEy6GHIMmJYAAAh+QQJCgADACwAAAAAGwAbAAACb5yPoRvofxSYFCgIpWX6YhNM3Ch+"+
      "4YhKzXOmqeW07ruC24zCNq5Riy5zVXynQfA1JG5uyKRvyQwOQ5WW6HelZpM9BnPq7DadSiKH"+
      "zDVWveiqrQ3XqePk2pyeTuCnmX0JoxVnlxEYNmhSePWByLFYAAAh+QQJCgADACwAAAAAGwAb"+
      "AAACbZyPoRvofxSYFCgIpWX6YhNM3Ch+4YhKzXOmYfWu0ZZWtpXQ5A3PLn9btVC2V3EwJFIW"+
      "PNUPBnTWgMWkkiqyXqlSlzGq7WJx2qUmCtIxGVhZmI2eZXdx+fzczIyDpv3yA/LVJAOIJJhV"+
      "mDECWAAAIfkECQoAAwAsAAAAABsAGwAAAm6cj6Eb6H8UmBQoCKXV9WITTEtFWl/IlGXzoKO6"+
      "Oi4Hkyy40VZt4kxIecFYMx2w1ijqeEHlkin5CZki5xRqlR5VUa0WmfUOB+Ei1yeuboORnNQd"+
      "a3vfNpk73cncf3H9/vxBBvTkEQiyVmWYIRVYAAAh+QQJCgADACwAAAAAGwAbAAACb5yPoRvo"+
      "fxSYFCgIZd22YRNw4uRl41km55oOobZy6RuP5RtWee3hOsxydXakGkWyMNpwwA2DSGI2o0+n"+
      "tPlk/IbZLHfb7R6BjWtYegRxz15SZM2GzeDxtCqudT/MYb2JnteylyPW8eEwZXiYkXVYAAA7";
    var innSpan = msg.appendChild(document.createElement("k8_msg_span"));
    if (typeof(opera) != "undefined") innSpan.setAttribute("style", "display:inline-block;padding:0 6px 0 8px;");
    else innSpan.setAttribute("style", "display:inline;padding:0 6px 0 4px;");
    msg.k8InnTN = innSpan.appendChild(document.createTextNode(""));
    db.insertBefore(msg, fc);
  }
  msg.k8InnTN.nodeValue = msgs+"";
  var wx = Math.ceil((window.innerWidth-msg.offsetWidth)/2);
  var wy = Math.ceil((window.innerHeight-msg.offsetHeight)/2);
  msg.style.left = wx+"px";
  msg.style.top = wy+"px";
  if (clickToRemove) {
    msg.onclick = function () { removeMessage(true); };
    msg.k8ClickToRemove = true;
  } else {
    msg.onclick = function () {};
    if (msg.k8ClickToRemove) delete msg.k8ClickToRemove;
  }
  var fnrs = function () {
    var wx = Math.ceil((window.innerWidth-msg.offsetWidth)/2);
    var wy = Math.ceil((window.innerHeight-msg.offsetHeight)/2);
    msg.style.left = wx+"px";
    msg.style.top = wy+"px";
  };
  msg.k8FNRS = fnrs;
  window.addEventListener("resize", fnrs, false);
}


function blockPage () {
  var i = document.getElementById("k8PageBlocker");
  if (i) return;
  var db = document.body;
  var ovr = document.createElement("div");
  ovr.setAttribute("style", "z-index:6666;opacity:0.5;width:100%;height:100%;display:block;position:fixed;left:0;top:0;background:#000");
  ovr.id = "k8PageBlocker";
  var fc = document.body.firstChild;
  db.insertBefore(ovr, fc);
}


function unblockPage () {
  removeMessage(false);
  var i = document.getElementById("k8PageBlocker");
  if (!i) return;
  i.style.display = "none";
  setTimeout(function () { i.parentNode.removeChild(i); }, 1);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// image builders

function convertPalette () {
  function toHC (n) { return (n>15?"":"0")+(n.toString(16)); }
  mainPal = [];
  var pd = me.data.palmain;
  //opera.postError(pd.length);
  for (var f = 0; f < 256; f++) mainPal[f] = "#"+toHC(pd[f*3+0]*4)+toHC(pd[f*3+1]*4)+toHC(pd[f*3+2]*4);
}


function buildImageMasked (darr, dpos, w, h) {
  var cc = document.createElement("canvas");
  cc.setAttribute("width", w);
  cc.setAttribute("height", h);
  var dc = cc.getContext("2d");
  var pl = me.data.palmain;
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      var c = darr[dpos++];
      if (!c) continue;
      dc.fillStyle = mainPal[c];
      dc.fillRect(x, y, 1, 1);
    }
  }
  img = document.createElement("img");
  img.setAttribute("width", w);
  img.setAttribute("height", h);
  img.src = cc.toDataURL();
  return img;
}


function buildImageMaskedShiny (darr, dpos, brightness, w, h) {
  var cc = document.createElement("canvas");
  cc.setAttribute("width", w);
  cc.setAttribute("height", h);
  var dc = cc.getContext("2d");
  var pl = me.data.palmain;
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      var c = darr[dpos++];
      if (!c) continue;
      var b = (c&15)-brightness;
      if (b < 0) b = 0; else if (b > 15) b = 15;
      dc.fillStyle = mainPal[(c&240)|b];
      dc.fillRect(x, y, 1, 1);
    }
  }
  img = document.createElement("img");
  img.setAttribute("width", w);
  img.setAttribute("height", h);
  img.src = cc.toDataURL();
  return img;
}


function buildImageMaskedInk (darr, dpos, ink, w, h) {
  ink = (ink||0); if (ink < 0) ink = 0; ink *= 16;
  var cc = document.createElement("canvas");
  cc.setAttribute("width", w);
  cc.setAttribute("height", h);
  var dc = cc.getContext("2d");
  var pl = me.data.palmain;
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      var c = darr[dpos++];
      if (!c) continue;
      dc.fillStyle = mainPal[c+ink];
      dc.fillRect(x, y, 1, 1);
    }
  }
  img = document.createElement("img");
  img.setAttribute("width", w);
  img.setAttribute("height", h);
  img.src = cc.toDataURL();
  return img;
}


function buildImageBrk (darr, dpos, ink, paper, skip, w, h, rep, masked) {
  skip = skip||0; ink *= 16; w = w||8; h = h||8; rep = rep||1;
  var cc = document.createElement("canvas");
  cc.setAttribute("width", w*rep);
  cc.setAttribute("height", h);
  var dc = cc.getContext("2d");
  var pl = me.data.palmain;
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      var c = y>=skip?darr[dpos++]:0;
      if (!c && masked) continue;
      dc.fillStyle = mainPal[c?ink+c:paper];
      for (var r = 0; r < rep; r++) dc.fillRect(x+r*w, y, 1, 1);
    }
  }
  img = document.createElement("img");
  img.setAttribute("width", w*rep);
  img.setAttribute("height", h);
  img.src = cc.toDataURL();
  return img;
}


function buildWilly () {
  var cc = document.createElement("canvas");
  cc.setAttribute("width", 16*16);
  cc.setAttribute("height", 16+16);
  var dc = cc.getContext("2d");
  var ww = me.data.willy, pl = me.data.palmain;
  for (var f = 0; f < 16; f++) {
    for (var y = 0; y < 16; y++) {
      for (var x = 0; x < 16; x++) {
        var c = me.data.willy[f*256+y*16+x];
        if (!c) continue;
        dc.fillStyle = mainPal[c];
        dc.fillRect(f*16+x, y, 1, 1);
        // white
        dc.fillStyle = mainPal[15];
        dc.fillRect(f*16+x, y+16, 1, 1);
      }
    }
  }
  willySpr = document.createElement("img");
  willySpr.setAttribute("width", 16*16);
  willySpr.setAttribute("height", 16+16);
  willySpr.src = cc.toDataURL();
}


/////////////////////////////////////////////////////////////////////////
// image cachers

function cacheBrickImages () {
  function buildBrickImage (brk, skip) {
    return buildImageBrk(me.data.blocks, brk.gfx*64, brk.ink, curRoom.paper, skip||0);
  }
  brickCache = [];
  for (var f = 0; f <= 7; f++) {
    var r;
    switch (f) {
      //case 0: case 7: r = buildBrickImage(curRoom.wall, 16); break;
      case 1: r = buildBrickImage(curRoom.platforms[0]); break;
      case 2: r = buildBrickImage(curRoom.platforms[1]); break;
      case 3: r = buildBrickImage(curRoom.wall); break;
      //case 4: r = buildBrickImage(curRoom.crumb); break;
      case 5: r = buildBrickImage(curRoom.deadlies[0]); break;
      case 6: r = buildBrickImage(curRoom.deadlies[1]); break;
    }
    brickCache.push(r);
  }
  for (var f = 0; f <= 8; f++) brickCache.push(buildBrickImage(curRoom.crumb, f));
}


function cacheConvImages () {
  convCache = []; //monsterCache = [];
  if (conv.y <= 0 || conv.l < 1) return;
  for (var f = 0; f <= 3; f++) {
    convCache.push(buildImageBrk(me.data.conv, conv.gfx*256+f*64, conv.ink, curRoom.paper, 0, 8, 8, conv.l));
  }
}


function cacheExit () {
  exitCache = [];
  exitCache.push(buildImageMasked(me.data.exits, curRoom.exit.gfx*256, 16, 16));
  for (var f = 0; f <= 15; f++) exitCache.push(buildImageMaskedShiny(me.data.exits, curRoom.exit.gfx*256, f, 16, 16));
}


function cacheKeys () {
  keyCache = [];
  for (var f = 0; f <= 15; f++) keyCache.push(buildImageMaskedShiny(me.data.keys, curRoom.keys.gfx*64, f, 8, 8));
}


function cacheMonsters () {
  monsterCache = []; monsterOfsCache = [];
  var l = curMonsters.length;
  for (var f = 0; f < l; f++) {
    var m = curMonsters[f];
    if (m.x < 0 || m.y < 0) continue;
    var r = [], cc = [];
    if (m.vert) {
      for (var c = 0; c <= 3; c++) {
        var n = (m.gfx+c)*256;
        cc.push(n);
        r.push(buildImageMaskedInk(me.data.vrobo, n, m.ink-1, 16, 16));
      }
    } else {
      for (var c = 0; c <= m.anim>>1; c++) {
        var n = (m.gfx+c)*256;
        cc.push(n);
        r.push(buildImageMaskedInk(me.data.hrobo, n, m.ink-1, 16, 16));
        n += m.flip*256;
        cc.push(n);
        r.push(buildImageMaskedInk(me.data.hrobo, n, m.ink-1, 16, 16));
      }
    }
    monsterOfsCache.push(cc);
    monsterCache.push(r);
  }
}


function cacheEugene () {
  eugeneSpr = [];
  for (var f = 0; f <= 256; f += 256)
    for (var c = 0; c <= 7; c++)
      eugeneSpr.push(buildImageMaskedInk(me.data.eugene, f, c, 16, 16));
}


function cacheKong () {
  kongSpr = [];
  for (var f = 0; f <= 3; f++) {
    for (var c = 0; c <= 7; c++) {
      kongSpr.push(buildImageMaskedInk(me.data.kong, f*256, c, 16, 16));
    }
  }
}


function cacheSkyLab () {
  skylabSpr = [];
  for (var f = 0; f <= 7; f++)
    for (var c = 0; c <= 7; c++)
      skylabSpr.push(buildImageMaskedInk(me.data.sky, f*256, c, 16, 16));
}


function cacheSwitch () {
  switchesSpr = [];
  for (var f = 0; f < 2; f++) {
    switchesSpr.push(buildImageBrk(me.data.switches, f*64, curRoom.platforms[1].ink, curRoom.paper, 0));
  }
}


/////////////////////////////////////////////////////////////////////////
// painters

function eraseRect (x0, y0, w, h) {
  ctx.fillStyle = mainPal[curRoom.paper];
  ctx.fillRect(x0, y0, w, h);
  var xe = x0+w, ye = y0+h;
  for (var dy = y0; dy <= ye; dy += 8) {
    for (var dx = x0; dx <= xe; dx += 8) {
      var x = dx>>3, y = dy>>3;
      if (x < 0 || y < 0 || x > 31 || y > 15) continue;
      var blk = curMap[y*32+x];
      if (blk < 1 || blk == 7) continue;
      if (blk == 4) blk = 8;
      ctx.drawImage(brickCache[blk], 0, 0, 8, 8, x*8, y*8, 8, 8);
    }
  }
}


function drawRoom () {
  var pos = 0;
  ctx.fillStyle = mainPal[curRoom.paper];
  ctx.fillRect(0, 0, 32*8, 16*8);
  for (var y = 0; y < 16; y++) {
    for (var x = 0; x < 32; x++) {
      var blk = curMap[pos++];
      if (blk < 1 || blk == 7) continue;
      if (blk == 4) blk = 8;
      ctx.drawImage(brickCache[blk], 0, 0, 8, 8, x*8, y*8, 8, 8);
    }
  }
  if (curRoomNo == 19) {
    ctx.drawImage(finalSpr, 0, 0, 256, 64, 0, 0, 256, 64);
    ctx.drawImage(sunSpr, 0, 0, 24, 16, 60, 32, 24, 16);
  }
}


function drawConveyor () {
  if (conv.l < 1) return;
  var y = conv.y;
  if (y <= 0) return;
  ctx.drawImage(convCache[conv.frame], 0, 0, conv.l*8, 8, conv.x, conv.y, conv.l*8, 8);
}


function drawExit () {
  if (keysLeft) br = 0;
  else {
    br = frameNo&31;
    if (br > 15) br = 31-br;
    br++;
  }
  ctx.drawImage(exitCache[br], 0, 0, 16, 16, curRoom.exit.x, curRoom.exit.y, 16, 16);
}


function drawKeys () {
  //if (!keysLeft) return;
  var ff = frameNo%16;
  if (ff >= 8) ff = 15-ff;
  var img = keyCache[ff];
  for (var f = curKeys.length-1; f >= 0; f--) {
    var ki = curKeys[f];
    if (ki.x < 0 || ki.y < 0) continue;
    eraseRect(ki.x, ki.y, 8, 8);
    if (!ki.s) continue;
    ctx.drawImage(img, 0, 0, 8, 8, ki.x&248, ki.y, 8, 8);
  }
}


function eraseMonsters () {
  var l = curMonsters.length;
  for (var f = 0; f < l; f++) {
    var m = curMonsters[f];
    //if (m.x < 0 || m.y < 0) continue; // we can't kill monsters now
    var x = m.x; if (!m.vert) x = x&248;
    eraseRect(x, m.y, 16, 16);
  }
  if (eugene) eraseRect(eugene.x, eugene.y, 16, 16);
  if (kong) eraseRect(kong.x, kong.y, 16, 16);
  if (skylab) {
    for (var f = 2; f >= 0; f--) eraseRect(skylab[f].x, skylab[f].y, 16, 16);
  }
}


function drawMonsters () {
  //return;
  var l = curMonsters.length;
  for (var f = 0; f < l; f++) {
    var m = curMonsters[f];
    //if (m.x < 0 || m.y < 0) continue; // we can't kill monsters now
    var slist = monsterCache[f];
    var x = m.x;
    if (m.vert) {
      //for (var c = 0; c <= m.anim; c++) r.push(buildImageMaskedInk(me.data.vrobo, (m.gfx+c)*256, m.ink-1, 16, 16, false));
      ctx.drawImage(slist[m.anim], 0, 0, 16, 16, x, m.y, 16, 16);
    } else ctx.drawImage(slist[((x&m.anim)&0xfe)+m.dir], 0, 0, 16, 16, x&248, m.y, 16, 16);
    //} else ctx.drawImage(slist[frameNo&m.anim], 0, 0, 16, 16, x&248, m.y, 16, 16);
  }
  if (eugene) {
    ctx.drawImage(eugeneSpr[curLSet*8+eugene.ink], 0, 0, 16, 16, eugene.x, eugene.y, 16, 16);
  }
  if (kong) {
    ctx.drawImage(kongSpr[kong.frame*8+kong.ink], 0, 0, 16, 16, kong.x, kong.y, 16, 16);
  }
  if (skylab) {
    for (var f = 0; f < skylab.length; f++) {
      var sk = skylab[f];
      ctx.drawImage(skylabSpr[sk.frame*8+sk.ink], 0, 0, 16, 16, sk.x, sk.y, 16, 16);
    }
  }
}


function drawSwitch () {
  var l = switches.length;
  for (var f = 0; f < l; f++) {
    var ss = switches[f];
    ctx.drawImage(switchesSpr[ss.state], 0, 0, 8, 8, ss.x, ss.y, 8, 8);
  }
}


function drawSPG (noerase) {
  if (!spg) return;
  for (var f = 0; f < spg.length; f++) {
    var x = spg[f].x*8, y = spg[f].y*8;
    if (x < 0 || y < 0) break;
    ctx.fillStyle = mainPal[noerase?6:curRoom.paper];
    ctx.fillRect(x, y, 8, 8);
  }
}



function drawWilly () {
  var willyPos = (willyX&15)>>1;
  if (willyDir < 0) willyPos += 8;
  var wy = 0;
  if (DEBUG_COLDET && dbgColDetected) wy = 16;
  ctx.drawImage(willySpr, willyPos*16, wy, 16, 16, willyX&248, willyY, 16, 16);
}


/////////////////////////////////////////////////////////////////////////
// checkers

// x & y: in pixels
function getBlockAt (x, y, simplify) {
  x = x>>3, y = y>>3;
  if (x < 0 || y < 0 || x > 31 || y > 15) return 0; // empty
  var b = curMap[y*32+x];
  if (simplify === true) {
    if (b > 15) b = 0;
    else if (b > 7) b = 4;
    else if (b == 6) b = 5;
    else if (b == 2) b = 1;
  }
  return b;
}


function isWillyFalling () {
  if (willyY&7) return true;
  for (var dx = 0; dx <= 8; dx += 8) {
    var b = getBlockAt(willyX+dx, willyY+16, true);
    if (b > 0 && b != 5) return false;
  }
  return true;
}


function isWillyInDeadly () {
  for (var dx = 0; dx <= 8; dx += 8) {
    for (var dy = 0; dy <= 16; dy += 8) {
      if (getBlockAt(willyX+dx, willyY+dy, true) == 5) return true;
    }
  }
  return false;
}


function isWillyOnConv () {
  if (willyY&7) return false;
  var b0 = getBlockAt(willyX, willyY+16);
  var b1 = getBlockAt(willyX+8, willyY+16);
  return b0 == 7 || b1 == 7;
}


function checkExit () {
  if (keysLeft) return false;
  var x = curRoom.exit.x, y = curRoom.exit.y;
  return (willyX >= x-2 && willyX+10 <= x+18 && willyY >= y-5 && willyY+16 <= y+22);
}


// pixel-perfect collision detector
// fully stolen from Andy's sources %-)
var mpcGrid = new Array(256);
function pixelCheckMonster (rx, ry, darr, dpos) {
  var x, y, w;
  //
  rx -= willyX&248;
  ry -= willyY;
  if (rx < -15 || rx > 15 || ry < -15 || ry > 15) return false;
  // clear grid
  for (var f = 255; f >= 0; f--) mpcGrid[f] = 0;
  if (rx < 0) x = 0, rx = -rx, w = 16-rx; else x = rx, rx = 0, w = 16-x;
  // partial plot monster
  for (y = ry+15; y >= ry; y--) {
    if (y >= 0 && y < 16) {
      var gp = y*16+x, rp = dpos+(y-ry)*16+rx;
      for (var dx = 0; dx < w; dx++, gp++, rp++) mpcGrid[gp] = darr[rp];
    }
  }
  var warr = me.data.willy, wptr = ((willyX&15)>>1)*256+(willyDir<0?2048:0);
  // check for collision
  var mp = 0;
  for (x = 255; x >= 0; x--, wptr++, mp++) if (warr[wptr] && mpcGrid[mp]) return true;
  return 0;
}


function checkMonsters () {
  var l = curMonsters.length;
  for (var f = 0; f < l; f++) {
    var m = curMonsters[f], cc = monsterOfsCache[f];
    //if (m.x < 0 || m.y < 0) continue; // we can't kill monsters now
    if (m.vert) {
      if (pixelCheckMonster(m.x, m.y, me.data.vrobo, cc[m.anim])) return true;
    } else {
      if (pixelCheckMonster(m.x&248, m.y, me.data.hrobo, cc[((m.x&m.anim)&0xfe)+m.dir])) return true;
    }
  }
  // Eugene?
  if (eugene) {
    if (pixelCheckMonster(eugene.x, eugene.y, me.data.eugene, 256*curLSet)) return true;
  }
  // Kong?
  if (kong) {
    if (pixelCheckMonster(kong.x, kong.y, me.data.kong, 256*kong.frame)) return true;
  }
  // SkyLab?
  if (skylab) {
    for (var f = 2; f >= 0; f--) {
      var sk = skylab[f];
      if (pixelCheckMonster(sk.x, sk.y, me.data.sky, 256*sk.frame)) return true;
    }
  }
}


function checkSwitch () {
  var l = switches.length;
  for (var f = 0; f < l; f++) {
    var ss = switches[f];
    if (ss.state) continue;
    var x = ss.x, y = ss.y;
    if (x+7 >= willyX && y+7 >= willyY && x < willyX+8 && y < willyY+16) ss.state = 1;
  }
}


function checkSPG () {
  if (!spg) return;
  for (var f = 0; f < spg.length; f++) {
    var x = spg[f].x*8, y = spg[f].y*8;
    if (x < 0 || y < 0) break;
    if (x+7 >= willyX && x < willyX+8 && y+7 >= willyY && y < willyY+16) return true;
  }
  return false;
}



/////////////////////////////////////////////////////////////////////////
// doers

function spgCheckMonster (x, y) {
  x *= 8; y *= 8;
  for (var f = 0; f < curMonsters.length; f++) {
    var cm = curMonsters[f];
    var mx = cm.x, my = cm.y;
    if (x+7 >= mx && x < mx+15 && y+7 >= my && y < my+15) return true;
  }
  return false;
}


function buildSPG () {
  var x = 23, y = 0, done = false;
  var dir = 0, idx = 0;

  if (!spg) spg = [];

  function addXY (x, y) {
    idx++;
    while (spg.length < idx) spg.push({x:-1, y:-1});
    spg[idx-1].x = x; spg[idx-1].y = y;
  }

  do {
    var blockhit = curMap[y*32+x];
    var robohit = spgCheckMonster(x, y);

    if (blockhit && robohit) {
      addXY(-1, -1);
      done = true;
    } else if (!blockhit && robohit) {
      if (idx && spg[idx-1].x == x && spg[idx-1].y == y) {
        spg[idx-1].x = spg[idx-1].y = -1;
        done = true;
      } else addXY(x, y);
      dir ^= 1;
    } else if (!blockhit && !robohit) {
      addXY(x, y);
    } else if (blockhit && !robohit) {
      if (idx && spg[idx-1].x == x && spg[idx-1].y == y) {
        spg[idx-1].x = spg[idx-1].y = -1;
        done = true;
      }
      dir ^= 1;
    }

    if (!blockhit) {
      if (!dir) {
        y++;
        blockhit = curMap[y*32+x];
        if (y == 15 || blockhit) done = true;
      } else {
        x--;
        blockhit = curMap[y*32+x];
        if (x == 0 || blockhit) done = true;
      }
    } else {
      if (!dir) { x--; if (!x) done = true; }
      else { y++; if (++y == 15) done = true; }
    }
  } while (!done);
  addXY(-1, -1);
}


function doKeys () {
  if (!keysLeft) return;
  for (var f = curKeys.length-1; f >= 0; f--) {
    var ki = curKeys[f];
    if (!ki.s) continue;
    var kx = ki.x, ky = ki.y;
    if (kx+7 >= willyX && kx < willyX+10 && ky+7 >= willyY && ky < willyY+16) {
      ki.s = false; keysLeft--;
      score += 100;
    }
  }
}


function doCrumb () {
  if (willyY&7) return false;
  for (var f = 0; f <= 8; f += 8) {
    var x = willyX+f, y = willyY+16;
    var b = getBlockAt(x, y);
    if (b == 4) b = 8;
    if (b < 8) continue;
    x >>= 3; y >>= 3;
    if (++b > 15) b = 0;
    curMap[y*32+x] = b;
    eraseRect(x*8, y*8, 8, 8);
  }
}


function doMonsters () {
  var l = curMonsters.length;
  for (var f = 0; f < l; f++) {
    var m = curMonsters[f];
    //if (m.x < 0 || m.y < 0) continue; // we can't kill monsters now
    if (m.vert) {
      var y = m.y, spd = m.speed;
      if (m.dir != 0) {
        // up
        y -= spd;
        if (y < m.min || y < 0) y += spd, m.dir = 0;
      } else {
        // down
        y += spd;
        if (y > m.max) y -= spd, m.dir = 1;
      }
      m.y = y;
      m.anim = (m.anim+1)&3;
    } else {
      var x = m.x, spd = (2>>m.speed);
      if (m.dir != 0) {
        // left
        x -= spd;
        if (x < m.min) m.dir = 0, x += spd;
      } else {
        // right
        x += spd;
        if (x > m.max+6) m.dir = 1, x -= spd;
      }
      m.x = x;
    }
  }
  // Eugene
  if (eugene) {
    if (!keysLeft) {
      // no keys, Eugene tries to block the exit
      eugene.ink = (eugene.ink+1)&7;
      eugene.y += eugene.y<eugene.max?1:0;
    } else {
      if (eugene.dir != 0) {
        // up
        eugene.y--;
        if (eugene.y < eugene.min) eugene.dir = 0, eugene.y++;
      } else {
        // down
        eugene.y++;
        if (eugene.y > eugene.max) eugene.dir = 1, eugene.y--;
      }
    }
  }
  // Kong
  if (kong) {
    switch (kong.falling) {
      case 1: // just started
        curMap[2*32+15] = 0;
        curMap[2*32+16] = 0;
        eraseRect(16, 120, 16, 8);
        kong.falling = 2;
        break;
      case 2:
        kong.ink = 4; kong.frame += 2;
        kong.falling = 3;
        break;
      case 3:
        kong.y += 4;
        if (kong.y >= kong.max) kong = false, score += 100;
        if (!kong.delay) kong.delay = 4, kong.frame = ((kong.frame-1)&1)+2; else kong.delay--;
        break;
      default:
        if (!kong.delay) kong.delay = 8, kong.frame = (kong.frame+1)&1; else kong.delay--;
    }
  }
  // SkyLab
  if (skylab) {
    for (var f = 0; f < skylab.length; f++) {
      var sk = skylab[f];
      switch (sk.m) {
        case 0:
          sk.y += sk.s;
          if (sk.y > sk.max) {
            sk.y = sk.max;
            sk.m = 1;
            sk.frame++;
          }
          break;
        case 1:
          sk.frame++;
          if (sk.frame == 7) sk.m = 2;
          break;
        case 2:
          sk.p = (sk.p+1)&3;
          sk.x = skylabCoords[f][sk.p].x;
          sk.y = skylabCoords[f][sk.p].y;
          sk.frame = sk.m = 0;
          break;
      }
    }
  }
}


function doSwitch () {
  // hole?
  if (holeLen && switches.length && switches[0].state) {
    if (holeLen < 0) {
      curMonsters[1].max += 24;
      holeLen = 0;
    } else {
      holeY++;
      eraseRect(136, 88, 8, 16);
      ctx.fillStyle = mainPal[curRoom.paper];
      ctx.fillRect(136, 88+16-holeY, 8, holeY);
      if (holeY == 16) {
        curMap[11*32+17] = 0;
        curMap[12*32+17] = 0;
        holeLen = -1;
      }
    }
  }
  // Kong?
  if (kong && !kong.falling && switches.length > 1 && switches[1].state) kong.falling = 1;
}


function doWillyLeft () {
  if (willyDir > 0) { willyDir = -1; return true; }
  var xx = willyX-2;
  var b0 = getBlockAt(xx, willyY);
  var b1 = getBlockAt(xx, willyY+8);
  var b2 = willyY&7?getBlockAt(xx, willyY+16):b1;
  if (b0 == 3 || b1 == 3 || b2 == 3) return false;
  willyX -= 2;
  if (willyX < 0) willyX += 240;
  willyLastMoveDir = -1;
  return true;
}


function doWillyRight () {
  if (willyDir < 0) { willyDir = 1; return true; }
  if (willyX > 245) return false;
  var xx = willyX+10;
  var b0 = getBlockAt(xx, willyY);
  var b1 = getBlockAt(xx, willyY+8);
  var b2 = willyY&7?getBlockAt(xx, willyY+16):b1;
  if (b0 == 3 || b1 == 3 || b2 == 3) return false;
  willyX += 2;
  if (willyX > 240) willyX -= 240;
  willyLastMoveDir = 1;
  return true;
}


function doWillyJump () {
  if (!willyJump) return;
  willyY += willyJ[willyJump];
  var x = willyX, mv = false;
  if (willyJumpDir < 0) mv = doWillyLeft(); else if (willyJumpDir > 0) mv = doWillyRight();
  if (willyJump < 9) {
    willyFall = 0;
    // up
    var b0 = getBlockAt(x, willyY);
    var b1 = getBlockAt(x+8, willyY);
    if (b0 == 3 || b1 == 3) {
      // headboom! (apstenu %-)
      willyX = x; willyY -= willyJ[willyJump];
      willyJump = 0; // enough flying
      return;
    }
  } else {
    // down
    if (willyJump > 12) willyFall += willyJ[willyJump];
    if ((willyY&7) == 0) {
      var b0 = getBlockAt(willyX, willyY+16);
      var b1 = getBlockAt(willyX+8, willyY+16);
      if (b0 || b1) {
  if (b0 == 3 || b1 == 3) willyX = x;
        willyFall = 0; // can't fall too deep while jumping
        willyJump = 0; // enough flying
        if (b0 == 7 || b1 == 7) willyStall = 1; // conveyor?
        return;
      }
    }
  }
  willyJump++;
  if (willyJump > 18) willyJump = 0;
}


function doWillyActions () {
  if (willyDead) return;
  checkSwitch();
  if (isWillyInDeadly()) { willyDead = true; return; }
  if (!DEBUG_COLDET) {
    if (checkMonsters()) { willyDead = true; return; }
  }

  var wasJump = false;
  if (willyJump) {
    willyLastMoveDir = 0;
    doWillyJump();
    if (willyJump) return;
    wasJump = true;
  }

  var falling = isWillyFalling();
  if (!kDown && falling) {
    willyConv = willyStall = willyLastMoveDir = 0;
    willyFall += 4;
    willyY += 4;
    if (willyY > 112) willyY -= 112;
    return;
  }

  if (!falling && willyFall > 34) willyDead = true; // too high!
  var lfall = willyFall;
  willyFall = 0;

  if (willyDead) return;

  var dx = kLeft?-1:kRight?1:0;
  if (isWillyOnConv()) {
    var cdir = conv.d?1:-1;
    //dx==cdir,!dx,lastmove==cdir
    if (willyLastMoveDir == cdir || dx == cdir || !dx) willyConv = cdir, willyStall = 0; // was moving in conv. dir or standing
    if (!willyConv) {
      // Willy just steps on the conveyor, and Willy walking to the opposite side
      //opera.postError("cdir="+cdir+"; wj="+wasJump+"; lmd="+willyLastMoveDir+"; lf="+lfall+"; ws="+willyStall);
      willyStall = 0;
      if (wasJump && willyLastMoveDir == -cdir) willyConv = dx; // from jump, can do opposite
      else {
        willyConv = dx;
        if (lfall > 0 || !willyLastMoveDir) dx = 0, willyStall = 1; // lands on conveyor, not from jump
      }
    } else {
      // Willy was on conveyor
      dx = willyStall?0:willyConv;
    }
  } else willyConv = willyStall = 0;

  //if (willyConv) dx = willyConv;
  if (kUp && !wasJump) {
    willyConv = willyStall = willyLastMoveDir = 0;
    willyJumpDir = dx;
    willyJump = 1;
    doWillyJump();
    return;
  }
  if (kDown) willyY -= 8;
  willyLastMoveDir = 0;
  if (dx < 0) doWillyLeft(); else if (dx > 0) doWillyRight();
}


/////////////////////////////////////////////////////////////////////////
// game ticking

function frameDraw () {
  drawSPG(true);
  drawConveyor();
  drawKeys();
  drawMonsters();
  drawSwitch();
  if (keysLeft) drawExit();
  drawWilly();
  if (!keysLeft) drawExit();
}


var startRoom; // forward declaration
function gameStep () {
  gameRunning = true;
  if (dbgSingleStep && !dbgNF) return
  dbgNF = false;
  frameNo++;
  conv.frame += conv.d?-1:1;
  if (conv.frame < 0) conv.frame = 3; else if (conv.frame > 3) conv.frame = 0;

  // must erase SPG, Willy and monsters here -- before coords change
  drawSPG(false);
  eraseMonsters();
  eraseRect(willyX&248, willyY, 16, 16);
  if (!willyJump) doCrumb();
  doMonsters();
  doWillyActions();
  if (DEBUG_COLDET) {
    if (checkMonsters()) {
      dbgSingleStep = true; dbgNF = false;
      dbgColDetected = true;
    } else dbgColDetected = false;
  }

  doKeys();
  doSwitch();
  if (spg) buildSPG();
  frameDraw();

  if (willyDead) {
    // dead %-(
    gameRunning = false;
    if (gameTID) clearInterval(gameTID), gameTID = null;
    blockPage();
    message("you are dead!");
    setTimeout(startRoom, 1000);
    return;
  }

  if (checkExit()) {
    // room complete!
    gameRunning = false;
    if (gameTID) clearInterval(gameTID), gameTID = null;
    blockPage();
    message(curRoom.title+" complete!");
    curRoomNo++;
    if (curRoomNo >= me.lsets[curLSet].rooms.length) {
      curRoomNo = 0;
      curLSet++;
      if (curLSet >= me.lsets.length) curLSet = 0;
    }
    setTimeout(startRoom, 2000);
    return;
  }
}


/////////////////////////////////////////////////////////////////////////
// game initializers

function initRoom () {
  gameRunning = false;
  frameNo = 0;
  // copy map
  curMap = []; curRoom = me.lsets[curLSet].rooms[curRoomNo];
  for (var f = 0; f < 512; f++) {
    var c = curRoom.map[f];
    if (c == 4) c = 8; // 'crumb'
    else if (c < 1 || c > 7) c = 0; // emptyness
    curMap.push(c);
  }
  // copy keys
  conv = curRoom.conveyor; conv.frame = 0;
  var k = curRoom.keys;
  curKeys = [];
  for (var f = 0; f < k.info.length; f++) {
    var ki = k.info[f];
    if (ki.s && ki.x >= 0 && ki.y >= 0) curKeys.push({x:ki.x, y:ki.y, s:true});
  }
  keysLeft = curKeys.length;
  // copy monsters
  curMonsters = [];
  var mns = curRoom.enemies;
  for (var f = 0; f < mns.length; f++) {
    var mm = mns[f];
    if (mm.x < 0 || mm.y < 0) continue;
    curMonsters.push({
      vert:mm.vert, x:mm.x, y:mm.y, min:mm.min, max:mm.max, anim:mm.anim, dir:mm.d, speed:mm.s,
      gfx:mm.gfx, flip:mm.flip, ink:mm.ink
    });
  }
  // Eugene?
  if (curRoomNo == 4) {
    cacheEugene();
    eugene = { x:120, y:1, dir:0, min:1, max:87, ink:6 };
  } else eugene = eugeneSpr = false;
  // Kong?
  if (curRoomNo == 7 || curRoomNo == 11) {
    cacheKong();
    kong = { x:120, y:0, max:104, frame:0, ink:2, m:0 };
    holeLen = 2*8; holeY = 0;
  } else kong = kongSpr = holeLen = holeY = false;
  // SkyLab?
  if (curRoomNo == 13) {
    curMonsters = [];
    cacheSkyLab();
    skylab = [
      {p:0, s:4, ink:6, max:72, m:0, frame:0},
      {p:2, s:3, ink:5, max:56, m:0, frame:0},
      {p:1, s:1, ink:4, max:32, m:0, frame:0}
    ];
    for (var f = 0; f < 3; f++) {
      var s = skylab[f];
      s.x = skylabCoords[f][skylab[f].p].x;
      s.y = skylabCoords[f][skylab[f].p].y;
    }
  } else skylab = skylabSpr = false;
  // solar power generator
  spg = false;
  if (curRoomNo == 18) buildSPG();
  // switches
  switches = [];
  for (var f = 0; f < curRoom.switches.length; f++) {
    var ss = curRoom.switches[f];
    if (ss.x < 0 || ss.y < 0 || !ss.s) continue;
    switches.push({ x:ss.x, y:ss.y, state: ss.s-1 });
  }
  //opera.postError(switches.length);
  // caching
  cacheBrickImages();
  cacheConvImages();
  cacheExit();
  cacheKeys();
  cacheMonsters();
  cacheSwitch();
  // init willy
  willyX = curRoom.willy.x; willyY = curRoom.willy.y;
  willyDir = curRoom.willy.sd>0?-1:1;
  willyJump = willyFall = willyConv = willyStall = 0;
  willyDead = false;
  willyLastMoveDir = 0;
  // reset keys
  kLeft = kRight = kJump = kUp = kDown = false;
  dbgNF = dbgSingleStep = false;
}


startRoom = function () {
  gameRunning = false;
  blockPage();
  message("loading");
  setTimeout(function () {
    initRoom();
    drawRoom();
    frameDraw();
    message("entering "+curRoom.title);
    setTimeout(function () {
      removeMessage();
      unblockPage();
      gameTID = setInterval(gameStep, 50);
    }, 2000);
  }, 1);
}


/////////////////////////////////////////////////////////////////////////
// controls, etc

function kbdTogglePause () {
  if (!gameRunning) return;
  if (gameTID) clearInterval(gameTID), gameTID = null;
  else gameTID = setInterval(gameStep, 50);

  if (!gameTID) { blockPage(); message("paused"); } else { removeMessage(); unblockPage(); }
  //kLeft = kRight = kJump = false;
}


kbdActions = [
  { key: ("P").charCodeAt(0), action: kbdTogglePause },
  { key: ("K").charCodeAt(0), action: function () { keysLeft = 0; } },
  { key: ("N").charCodeAt(0), action: function () { keysLeft = 0; willyX = curRoom.exit.x; willyY = curRoom.exit.y; } },
  { key: ("R").charCodeAt(0), action: function () { willyDead = true; } },
  { key: ("S").charCodeAt(0), action: function () { dbgSingleStep = !dbgSingleStep; dbgNF = false; } },
  { key: ("C").charCodeAt(0), action: function () { DEBUG_COLDET = !DEBUG_COLDET; dbgColDetected = false; } },
  { key: (" ").charCodeAt(0), action: function () { dbgNF = true; }, upAction: function () { dbgNF = false; } },

  { key: 40, action: function () { kDown = true; }, upAction: function () { kDown = false; } },
  { key: 37, action: function () { kLeft = true; }, upAction: function () { kLeft = false; } },
  { key: 38, action: function () { kUp = true; }, upAction: function () { kUp = false; } },
  { key: 39, action: function () { kRight = true; }, upAction: function () { kRight = false; } },

  { key: 65500 }
];


function kbdFindKey (evt) {
  for (var f = kbdActions.length-1; f >= 0; f--) {
    var k = kbdActions[f];
    if (k.key != evt.keyCode) continue;
    if (typeof(k.ctrl) == "boolean" && k.ctrl != evt.ctrlKey) continue;
    if (typeof(k.alt) == "boolean" && k.alt != evt.altKey) continue;
    if (typeof(k.shift) == "boolean" && k.shift != evt.shiftKey) continue;
    return k;
  }
  return false;
}


function onKeyDown (evt) {
  if (evt.ctrlKey) kJump = true;
  var k = kbdFindKey(evt);
  if (!k) return true;
  evt.preventDefault();
  k.down = true;
  if (k.action) k.action();
  return false;
}


function onKeyUp (evt) {
  if (evt.ctrlKey) kJump = false;
  var k = kbdFindKey(evt);
  if (!k) return true;
  evt.preventDefault();
  k.down = false;
  if (k.upAction) k.upAction();
  return false;
}


function onKeyPress (evt) {
  //opera.postError("kp: "+evt.keyCode);
  if (!gameTID) return true;
  evt.preventDefault();
  return false;
}


function hookKeys () {
  for (var f in kbdActions) kbdActions[f].down = false;
  document.addEventListener("keydown", onKeyDown, false);
  document.addEventListener("keyup", onKeyUp, false);
  document.addEventListener("keypress", onKeyPress, false);
}


/* prepare images */
function loadData () {
  blockPage();
  message("initializing");
  setTimeout(function () {
    convertPalette();
    buildWilly();
    finalSpr = buildImageMasked(me.data.final, 0, 256, 64);
    sunSpr = buildImageMasked(me.data.sun, 0, 24, 16);
    hookKeys();
    gameTID = false;
    score = 0;
    curLSet = 0;
    curRoomNo = 0;
    setTimeout(startRoom, 10);
  }, 10);
}


function initCanvas () {
  canvas = document.createElement("canvas");
  canvas.setAttribute("width", 512);
  canvas.setAttribute("height", 256);
  ctx = canvas.getContext("2d");
  ctx.scale(2, 2);

  ctx.fillStyle = "#202020";
  //ctx.fillStyle = "rgba(32, 32, 32, 50)";
  ctx.fillRect(0, 0, 320, 200);

  mainX = Math.ceil((window.innerWidth-512)/2);
  mainY = Math.ceil((window.innerHeight-256)/2);

  //mainX = 20; mainY = 20;

  canvas.style.position = "fixed";
  canvas.style.left = mainX+"px";
  canvas.style.top = mainY+"px";

  //window.onresize = function () {
  window.addEventListener("resize", function () {
    mainX = Math.ceil((window.innerWidth-512)/2);
    mainY = Math.ceil((window.innerHeight-256)/2);
    canvas.style.position = "fixed";
    canvas.style.left = mainX+"px";
    canvas.style.top = mainY+"px";
  }, false);
}


this.run = function () {
  gameRunning = false;
  initCanvas();
  setTimeout(function () {
    document.body.appendChild(canvas);
    setTimeout(loadData, 10);
  }, 10);
};


return this;
}

window.miner = new ManicMiner();
