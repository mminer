function UnpackRoom (roomS) {
  var room = {}, pos = 0;

  function GetNum () {
    var n = roomS.charCodeAt(pos++)-48, len;
    var len = n>>4; n = n&0x0f;
    while (len--) n = n*10+roomS.charCodeAt(pos++)-48;
    return n;
  }

  function GetIP (arr) {
    arr.ink = GetNum();
    arr.paper = GetNum();
  }

  function GetGIP (arr) {
    arr.gfx = GetNum();
    GetIP(arr);
  }

  // map
  room.map = [];
  for (var f = 0; f < 512; ) {
    var bt = roomS.charCodeAt(pos++)-48;
    var cnt = roomS.charCodeAt(pos++)-31;
    while (cnt--) { room.map.push(bt); f++; }
  }
  // params and Willy start
  room.air = GetNum();
  room.willy = { x:GetNum(), y:GetNum(), sd:GetNum() };
  room.exit = { gfx:GetNum(), x:GetNum(), y:GetNum() };
  room.border = GetNum();
  GetIP(room);
  // platforms
  var cnt = GetNum();
  room.platforms = [];
  while (cnt--) {
    var t = {}; GetGIP(t);
    room.platforms.push(t);
  }
  room.wall = {}; GetGIP(room.wall);
  room.crumb = {}; GetGIP(room.crumb);
  // deadlies
  var cnt = GetNum();
  room.deadlies = [];
  while (cnt--) {
    var t = {}; GetGIP(t);
    room.deadlies.push(t);
  }
  // conveyor
  room.conveyor = { x:GetNum(), y:GetNum(), d:GetNum(), l:GetNum() };
  GetGIP(room.conveyor);
  // keys
  cnt = GetNum();
  //LogPrint("keys: "+cnt);
  room.keys = { gfx: GetNum(), info:[] };
  for (var f = 0; f < 5; f++) {
    var t = {};
    if (f < cnt) t = { x:GetNum(), y:GetNum(), s: GetNum() };
    else t = { x:-1, y:-1, s:0 };
    room.keys.info.push(t);
  }
  // switches
  cnt = GetNum();
  room.switches = [];
  for (var f = 0; f < 2; f++) {
    var sw;
    if (f < cnt) sw = { x: GetNum(), y:GetNum(), s:GetNum() };
    else sw = { x: 0, y: 0, s: 0 };
    room.switches.push(sw);
  }
  // enemies
  cnt = GetNum();
  room.enemies = [];
  for (var f = 0; f < 8; f++) {
    var e;
    if (f < cnt) {
      e = {};
      e.vert = roomS.charCodeAt(pos++)=="V";
      GetGIP(e);
      e.x = GetNum();
      e.y = GetNum();
      e.min = GetNum();
      e.max = GetNum();
      e.d = GetNum();
      e.s = GetNum();
      e.flip = GetNum();
      e.anim = GetNum();
    } else {
      e = { vert: true, gfx: 0, ink: 1, paper: 0, x: -1, y: -1, min: 2, max: 81, d: 0, s: 111, flip: 0, anim: 0 };
    }
    room.enemies.push(e);
  }
  // title
  room.title = roomS.substr(pos);

  return room;
}
