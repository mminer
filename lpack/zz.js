load("unpacker.js");
load("lset0.js");


function RoomDump (room) {

  function RoomDumpMap (room) {
    var xx = 32, s = "";
    var map = room.map;
    for (var f = 0; f < 512; f++) {
      s += map[f];
      if (f < 512-1) s += ",";
      if (!--xx) { print(s); s = ""; xx = 32; }
    }
    if (s) print("!"+s);
  }

  function RoomDumpArr2Str (a, ind) {
    ind = ind || 0;
    var s = "";
    while (ind-- > 0) s += " ";
    s += "{";
    var ch = "";
    for (k in a) {
      s += ch+k+":"+a[k];
      ch = ", ";
    }
    s += "}";
    return s;
  }

  function RoomDumpArr (name) {
    var s = "room."+name+" = "+RoomDumpArr2Str(room[name])+";";
    print(s);
  }

  function RoomDumpArrArr (name) {
    print("room."+name+" = [");
    var a = room[name];
    for (var f = 0; f < a.length; f++) {
      var s = RoomDumpArr2Str(a[f], 1);
      if (f < a.length-1) s += ",";
      print(s);
    }
    print("];");
  }

  function RoomDumpKeys () {
    var k = room.keys;
    print("room.keys = {");
    print(" gfx: "+k.gfx+",");
    print(" info: [");
    var a = k.info;
    for (var f = 0; f < a.length; f++) {
      var s = RoomDumpArr2Str(a[f], 2);
      if (f < a.length-1) s += ",";
      print(s);
    }
    print(" ]");
    print("};");
  }

  print("room.map = [");
  RoomDumpMap(room);
  print("];");
  print("room.title = \""+room.title+"\";");
  print("room.air = "+room.air+";");
  //print("room.willy = "+RoomDumpArr(room.willy)+";");
  RoomDumpArr("willy");
  RoomDumpArr("exit");
  print("room.border = "+room.border+";");
  print("room.ink = "+room.ink+";");
  print("room.paper = "+room.paper+";");
  RoomDumpArrArr("platforms");
  RoomDumpArr("wall");
  RoomDumpArr("crumb");
  RoomDumpArrArr("deadlies");
  RoomDumpArr("conveyor");
  RoomDumpKeys();
  RoomDumpArrArr("switches");
  RoomDumpArrArr("enemies");
}


var r = UnpackRoom(rooms[0]);
RoomDump(r);

/*
title: "Central Cavern",
willy: { x: 16, y: 104, sd: 0 },
exit: { gfx: 0, x: 232, y: 104 },
air: 224,
border: 2, ink: 0, paper: 0,
platforms: [
 { gfx: 0, ink: 2, paper: 0 },
 { gfx: 0, ink: 0, paper: 0 }
],
wall: { gfx: 11, ink: 8, paper: 2 },
crumb: { gfx: 7, ink: 2, paper: 0 },
deadlies: [
 { gfx: 26, ink: 4, paper: 0 },
 { gfx: 29, ink: 5, paper: 0 }
],
conveyor: { x: 64, y: 72, d: 0, l: 20, gfx: 0, ink: 4, paper: 0 },
keys: {
 gfx: 0,
 info: [
  { x: 72, y: 0, s: 1 },
  { x: 232, y: 0, s: 1 },
  { x: 128, y: 8, s: 1 },
  { x: 192, y: 32, s: 1 },
  { x: 240, y: 48, s: 1 }
 ]
},
switches: [
 { x: 0, y: 0, s: 0 },
 { x: 0, y: 0, s: 0 }
],
enemies: [
 { vert: false, gfx: 0, ink: 6, paper: 0, x: 64, y: 56, min: 64, max: 120, d: 0, s: 0, flip: 8, anim: 15 }, 
 { vert: false, gfx: 0, ink: 7, paper: 7, x: -1, y: -1, min: -1, max: -1, d: 255, s: 255, flip: 0, anim: 0 }, 
 { vert: false, gfx: 0, ink: 0, paper: 0, x: -1, y: -1, min: -1, max: -1, d: 255, s: 255, flip: 0, anim: 0 }, 
 { vert: false, gfx: 0, ink: 0, paper: 0, x: -1, y: -1, min: -1, max: -1, d: 255, s: 255, flip: 0, anim: 0 }, 
 { vert: true, gfx: 0, ink: 7, paper: 7, x: -1, y: -1, min: -1, max: -1, d: 255, s: 255, flip: 0, anim: 0 }, 
 { vert: true, gfx: 0, ink: 7, paper: 3, x: -1, y: -1, min: 120, max: 31, d: 1, s: 13, flip: 0, anim: 0 }, 
 { vert: true, gfx: 0, ink: 7, paper: 7, x: -1, y: -1, min: 0, max: 0, d: 1, s: 60, flip: 0, anim: 0 }, 
 { vert: true, gfx: 0, ink: 1, paper: 0, x: -1, y: -1, min: 2, max: 81, d: 0, s: 111, flip: 0, anim: 0 }
]},
*/
