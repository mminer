function PackMap (map) {
  var res = "", cnt, bt;

  for (var f = 0; f < map.length; ) {
    cnt = 32; bt = map[f++];
    while (cnt < 126 && f < map.length && map[f] == bt) { f++; cnt++; }
    res += String.fromCharCode(bt+48)+String.fromCharCode(cnt);
  }

  return res;
}


function LogPrint (s) {
  //!print(s);
}


function Num2Str (n) {
  var res = ""+n;

  if (n < 0) {
    print("fuckin' small!");
    print(n);
    fuckout();
  }
  if (res.length > 1) {
    if (res.length > 16) {
      print("fuckin' big!");
      print(n);
      fuckout();
    }
    var z = res.charCodeAt(0)+((res.length-1)<<4);
    if (z < 1 || z > 126) {
      print("fuckin' shit!");
      print(z);
      print(res.charCodeAt(0));
      print(res.length);
      fuckout();
    }
    res = String.fromCharCode(z)+res.substr(1);
  }

  return res;
}


function PackRoom (room) {
  var res = PackMap(room.map);

  function PutIP (arr) {
    res += Num2Str(arr.ink, 2);
    res += Num2Str(arr.paper, 2);
  }

  function PutGIP (arr) {
    res += Num2Str(arr.gfx, 3);
    PutIP(arr);
  }

  LogPrint("ROOM: "+room.title);
  //res += "\n";
  // air
  LogPrint("air");
  res += Num2Str(room.air, 3);
  // starting pos
  LogPrint("starting pos");
  res += Num2Str(room.willy.x, 3);
  res += Num2Str(room.willy.y, 3);
  res += Num2Str(room.willy.sd, 3);
  // exit
  LogPrint("exit");
  res += Num2Str(room.exit.gfx, 3);
  res += Num2Str(room.exit.x, 3);
  res += Num2Str(room.exit.y, 3);
  // colors
  LogPrint("colors");
  res += Num2Str(room.border, 1);
  PutIP(room);
  // platforms
  LogPrint("platforms");
  res += Num2Str(room.platforms.length, 1);
  for (var f = 0; f < room.platforms.length; f++) PutGIP(room.platforms[f]);
  // wall
  LogPrint("wall");
  PutGIP(room.wall);
  // crumb
  LogPrint("crumb");
  PutGIP(room.crumb);
  // deadlies
  LogPrint("deadlies");
  res += Num2Str(room.deadlies.length, 1);
  for (var f = 0; f < room.deadlies.length; f++) PutGIP(room.deadlies[f]);
  // conveyor
  LogPrint("conveyor");
  res += Num2Str(room.conveyor.x, 3);
  res += Num2Str(room.conveyor.y, 3);
  res += Num2Str(room.conveyor.d, 1);
  res += Num2Str(room.conveyor.l, 2);
  PutGIP(room.conveyor);
  // keys
  LogPrint("keys");
  var ki = room.keys;
  var c = 0; for (var f = 0; f < ki.info.length; f++) if (ki.info[f].x >= 0) c++;
  res += Num2Str(c, 1);
  res += Num2Str(ki.gfx, 3);
  ki = ki.info;
  for (var f = 0; f < ki.length; f++) {
    if (ki[f].x < 0) continue;
    res += Num2Str(ki[f].x, 3);
    res += Num2Str(ki[f].y, 3);
    res += Num2Str(ki[f].s, 1);
  }
  // switches
  LogPrint("switches");
  var c = 0; for (var f = 0; f < room.switches.length; f++) if (room.switches[f].x > 0) c++;
  res += Num2Str(c, 1);
  for (var f = 0; f < room.switches.length; f++) {
    if (room.switches[f].x <= 0) continue;
    res += Num2Str(room.switches[f].x, 3);
    res += Num2Str(room.switches[f].y, 3);
    res += Num2Str(room.switches[f].s, 1);
  }
  // enemies
  LogPrint("enemies");
  var c = 0; for (var f = 0; f < room.enemies.length; f++) if (room.enemies[f].x >= 0) c++;
  res += Num2Str(c, 1);
  for (var f = 0; f < room.enemies.length; f++) {
    if (room.enemies[f].x <= 0) continue;
    res += room.enemies[f].vert?"V":"H";
    PutGIP(room.enemies[f]);
    res += Num2Str(room.enemies[f].x, 3);
    res += Num2Str(room.enemies[f].y, 3);
    res += Num2Str(room.enemies[f].min, 3);
    res += Num2Str(room.enemies[f].max, 3);
    res += Num2Str(room.enemies[f].d, 1);
    res += Num2Str(room.enemies[f].s, 3);
    res += Num2Str(room.enemies[f].flip, 1);
    res += Num2Str(room.enemies[f].anim, 2);
  }
  // title
  res += room.title;

  return res;
}
