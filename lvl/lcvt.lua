local function Cvt (setNo)

local fl = assert(io.open("set"..setNo..".lev"));
local s = fl:read("*a");
fl:close();

local function GetByte (name)
  local b = s:byte(1, 1);
  s = s:sub(2, -1);
  return b;
end;

local function GetWord (name)
  local b = GetByte(); b = b+256*GetByte();
  if b > 32767 then b = b-65536; end;
  return b;
end;

local function GetIPG (name)
  local res = { ink=GetByte(), paper=GetByte(), gfx=GetByte() };
  return res;
end;


local function SaveLevel (fo, level)
  local function WriteEnemy (e, vert)
    fo:write(" { vert: ", e.vert and "true" or "false", ", ");
    fo:write("gfx: ", e.gfx, ", ");
    fo:write("ink: ", e.ink, ", ");
    fo:write("paper: ", e.paper, ", ");
    fo:write("x: ", e.x, ", ");
    fo:write("y: ", e.y, ", ");
    fo:write("min: ", e.min, ", ");
    fo:write("max: ", e.max, ", ");
    fo:write("d: ", e.d, ", ");
    fo:write("s: ", e.s, ", ");
    fo:write("flip: ", e.flip, ", ");
    fo:write("anim: ", e.anim, " }");
  end;

  fo:write('{\ntitle: "', level.title, '",\n');
  fo:write("willy: { x: ", level.willy_x, ", y: ", level.willy_y, ", sd: ", level.willy_sd, " },\n");
  local p = level.exit;
  fo:write("exit: { gfx: ", p.gfx, ", x: ", p.x, ", y: ", p.y, " },\n");
  fo:write("air: ", level.air, ",\n");
  fo:write("map: [\n");
  for y = 0, 15 do
    for x = 0, 31 do
      local b = level.map[y*32+x+1];
      assert(b >= 0 and b <= 7);
      fo:write(string.format("%i", b));
      if x ~= 31 then fo:write(","); end;
    end;
    if y ~= 15 then fo:write(",\n"); end;
  end;
  fo:write("],\n");
  fo:write("border: ", level.border, ", ink: ", level.ink, ", paper: ", level.paper, ",\n");
  fo:write("platforms: [\n");
  for f = 1, 2 do
    local p = level.platforms[f];
    if f ~= 1 then fo:write(",\n"); end;
    fo:write(" { gfx: ", p.gfx, ", ink: ", p.ink, ", paper: ", p.paper, " }");
  end;
  fo:write("\n],\n");
  local p = level.wall;
  fo:write("wall: { gfx: ", p.gfx, ", ink: ", p.ink, ", paper: ", p.paper, " },\n");
  local p = level.crumb;
  fo:write("crumb: { gfx: ", p.gfx, ", ink: ", p.ink, ", paper: ", p.paper, " },\n");
  fo:write("deadlies: [\n");
  for f = 1, 2 do
    local p = level.kills[f];
    if f ~= 1 then fo:write(",\n"); end;
    fo:write(" { gfx: ", p.gfx, ", ink: ", p.ink, ", paper: ", p.paper, " }");
  end;
  fo:write("\n],\n");
  local p = level.conv;
  fo:write("conveyor: { x: ", p.x, ", y: ", p.y, ", d: ", p.d, ", l: ", p.l, ", gfx: ", p.gfx, ", ink: ", p.ink, ", paper: ", p.paper, " },\n");
  local p = level.keys;
  fo:write("keys: {\n gfx: ", p.gfx, ",\n info: [");
  for f = 1, 5 do
    if f ~= 1 then fo:write(","); end;
    fo:write("\n  { x: ", p[f].x, ", y: ", p[f].y, ", s: ", p[f].s, " }");
  end;
  fo:write("\n ]\n},\n");
  fo:write("switches: [\n");
  for f = 1, 2 do
    local p = level.switches[f];
    if f ~= 1 then fo:write(",\n"); end;
    fo:write(" { x: ", p.x, ", y: ", p.y, ", s: ", p.s, " }");
  end;
  fo:write("\n],\n");
  fo:write("enemies: [\n");
  for f = 1, 8 do
    local p = level.enemies[f];
    if f ~= 1 then fo:write(", \n"); end;
    WriteEnemy(p);
  end;
  fo:write("\n]}");
end;


if s:sub(1, 8) ~= "MANIC\13\10\26" then print("BAD!"); return 1; end;
s = s:sub(9, -1);

local levels = {};
local ln = s:sub(1, 16):match("^[%z%s]*(.-)[%z%s]*$"); s = s:sub(17, -1);
print("level set: "..ln);

local fo = assert(io.open("levelset"..setNo..".js", "w"));
fo:write('window.miner.AddLevelSet(\n{ name: "', ln, '", rooms: [\n');

for lvno = 1, 20 do
  if lvno ~= 1 then fo:write(",\n\n"); end;
  -- map: 32x16
  local level, map = {}, {};
  for c = 1, 512 do map[#map+1] = GetByte(); end;
  local title = s:sub(1, 33):match("^[%z%s]*(.-)[%z%s]*$"); s = s:sub(34, -1);
  level.title = title;
  level.map = map;
  level.border = GetByte("border");
  level.ink = GetByte("ink");
  level.paper = GetByte("paper");
  level.platforms = { GetIPG("plat1"), GetIPG("plat2") };
  level.wall = GetIPG("wall");
  level.crumb = GetIPG("crumb");
  level.kills = { GetIPG("kill1"), GetIPG("kill2") };
  level.conv = GetIPG("conveyor");
  level.willy_x = GetWord("willy_x");
  level.willy_y = GetWord("willy_y");
  level.willy_sd = GetByte("willy_sd");
  level.conv.x = GetWord("conveyor_x");
  level.conv.y = GetWord("conveyor_y");
  level.conv.d = GetByte("conveyor_d");
  level.conv.l = GetByte("conveyor_l");

  level.keys = {}; for c = 1, 5 do level.keys[c] = {}; end;
  for c = 1, 5 do level.keys[c].x = GetWord("key"..c.."_x"); end;
  for c = 1, 5 do level.keys[c].y = GetWord("key"..c.."_y"); end;
  level.keys.gfx = GetByte("key_gfx");
  for c = 1, 5 do level.keys[c].s = GetByte("key"..c.."_s"); end;

  level.switches = {}; for c = 1, 2 do level.switches[c] = {}; end;
  for c = 1, 2 do level.switches[c].x = GetWord("switch"..c.."_x"); end;
  for c = 1, 2 do level.switches[c].y = GetWord("switch"..c.."_y"); end;
  for c = 1, 2 do level.switches[c].s = GetByte("switch"..c.."_s"); end;

  level.exit = { x=GetWord("exit_x"), y=GetWord("exit_y"), gfx = GetByte("exit_gfx") };

  level.air = GetByte("air");

  -- enemies
  level.enemies = {};
  for f = 1, 2 do
    local ee = {}; for c = 1, 4 do ee[c] = {}; end;
    local ename = (f==1 and "h_" or "v_").."enemy";
    for c = 1, 4 do ee[c].vert = f == 2; end;
    for c = 1, 4 do ee[c].ink = GetByte(ename..c.."_ink"); end;
    for c = 1, 4 do ee[c].paper = GetByte(ename..c.."_paper"); end;
    for c = 1, 4 do ee[c].x = GetWord(ename..c.."_x"); end;
    for c = 1, 4 do ee[c].y = GetWord(ename..c.."_y"); end;
    for c = 1, 4 do ee[c].min = GetWord(ename..c.."_min"); end;
    for c = 1, 4 do ee[c].max = GetWord(ename..c.."_max"); end;
    for c = 1, 4 do ee[c].d = GetByte(ename..c.."_d"); end;
    for c = 1, 4 do ee[c].s = GetByte(ename..c.."_s"); end;
    for c = 1, 4 do ee[c].gfx = GetWord(ename..c.."_gfx"); end;
    for c = 1, 4 do ee[c].flip = 0; end;
    if f == 1 then for c = 1, 4 do ee[c].flip = GetByte(ename..c.."_flip"); end; end;
    for c = 1, 4 do ee[c].anim = GetByte(ename..c.."_anim"); end;
    --level.enemies[f] = ee;
    for c = 1, 4 do level.enemies[#level.enemies+1] = ee[c]; end;
  end;

  --print(title.."|");
  SaveLevel(fo, level);
  print(string.format("%2i: %s", lvno, title));
  --break;
end;
fo:write("\n]});\n");
fo:close();
end;

Cvt(0);
Cvt(1);
